From 10fe816b763c41099fa1c978a79d6869246671cf Mon Sep 17 00:00:00 2001
From: Cordell Bloor <Cordell.Bloor@amd.com>
Date: Thu, 19 Dec 2024 23:18:40 -0700
Subject: [PATCH] Check ROCm library versions directly

There is no guarantee that the version number of the rocm-core library
will match that of other ROCm libraries. It is both simpler and more
robust to directly check the version number of the relevent library.
For this reason, Debian has declined to package rocm-core. All
other AMD GPU dependencies of magma are available in Debian Trixie.

The Debian builds of magama-rocm currently use libamdhip64-dev 5.7.1,
libhipblas-dev 5.5.1, and libhipsparse-dev 5.7.1. The Ubuntu 24.04 LTS
was frozen with those package versions in its universe repositories.
There is no single value that could accurately describe the ROCM_VERSION
on Debian or Ubuntu.

At the moment, a failure to find rocm-core is treated by magma as a
version less than 6.0.0. Without this change, the Debian build of
magma will begin to fail when either of the libamdhip64-dev or
libhipblas-dev libraries are updated to a newer version.

Forwarded: https://github.com/icl-utk-edu/magma/pull/27
---
 CMakeLists.txt                              |  4 --
 Makefile                                    |  1 -
 interface_cuda/blas_z_v2.cpp                |  2 +-
 interface_cuda/interface.cpp                |  2 +-
 make.inc-examples/make.inc.hip-gcc-mkl      |  3 -
 make.inc-examples/make.inc.hip-gcc-openblas |  3 -
 tools/get-rocm-version.sh                   | 68 ---------------------
 7 files changed, 2 insertions(+), 81 deletions(-)
 delete mode 100755 tools/get-rocm-version.sh

diff --git a/CMakeLists.txt b/CMakeLists.txt
index fc61af74d..ba180e23c 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -304,10 +304,6 @@ if (MAGMA_ENABLE_HIP)
 
     #add_compile_options(${GPU_ARCH_FLAGS})
     set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D__HIP_PLATFORM_AMD__" )
-    option(ROCM_CORE "Location of the rocm-core package")
-    execute_process(COMMAND "${CMAKE_SOURCE_DIR}/tools/get-rocm-version.sh" "${ROCM_CORE}" OUTPUT_VARIABLE ROCM_VERSION OUTPUT_STRIP_TRAILING_WHITESPACE)
-    message(STATUS "ROCM_VERSION=${ROCM_VERSION}")
-    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DROCM_VERSION=${ROCM_VERSION}" )
   else()
     message( STATUS "Could not find HIP" )
   endif()
diff --git a/Makefile b/Makefile
index 44880ab61..1fabf54ca 100644
--- a/Makefile
+++ b/Makefile
@@ -847,7 +847,6 @@ d_ext := cu
 else ifeq ($(BACKEND),hip)
 d_ext := cpp
 CXXFLAGS += -D__HIP_PLATFORM_AMD__
-CXXFLAGS += -DROCM_VERSION=$(shell ./tools/get-rocm-version.sh)
 endif
 
 
diff --git a/interface_cuda/blas_z_v2.cpp b/interface_cuda/blas_z_v2.cpp
index 351ca7a7e..c7f5da2e0 100644
--- a/interface_cuda/blas_z_v2.cpp
+++ b/interface_cuda/blas_z_v2.cpp
@@ -1859,7 +1859,7 @@ magma_ztrmm(
 		    int(m), int(n),
 		    (cuDoubleComplex*)&alpha, (const cuDoubleComplex*)dA, int(ldda),
 		    (cuDoubleComplex*)dB, int(lddb)
-    #if (ROCM_VERSION >= 60000)
+    #if (hipblasVersionMajor >= 2)
 		    , (cuDoubleComplex*)dB, int(lddb)
     #endif
 		    );
diff --git a/interface_cuda/interface.cpp b/interface_cuda/interface.cpp
index 50b4fa3d3..f2495e5ad 100644
--- a/interface_cuda/interface.cpp
+++ b/interface_cuda/interface.cpp
@@ -540,7 +540,7 @@ magma_is_devptr( const void* A )
                   #endif
 
                 #elif defined(MAGMA_HAVE_HIP)
-		  #if ROCM_VERSION >= 60000
+		  #if HIP_VERSION_MAJOR >= 6
 		    return (attr.type == hipMemoryTypeDevice);
 		  #else
                     return (attr.memoryType == hipMemoryTypeDevice);
diff --git a/make.inc-examples/make.inc.hip-gcc-mkl b/make.inc-examples/make.inc.hip-gcc-mkl
index 4a7809e20..24ee6bf6b 100644
--- a/make.inc-examples/make.inc.hip-gcc-mkl
+++ b/make.inc-examples/make.inc.hip-gcc-mkl
@@ -104,9 +104,6 @@ LDFLAGS     =     $(FPIC) $(FOPENMP)
 # add in the default for device compiling
 DEVCCFLAGS  = -O3 -DNDEBUG -DADD_
 
-# Add ROCM_VERSION for device compilation
-DEVCCFLAGS += -DROCM_VERSION=$(shell ./tools/get-rocm-version.sh)
-
 # add the flags in a backend-specific way
 ifeq ($(BACKEND),cuda)
     DEVCCFLAGS += -Xcompiler "$(FPIC)" -Xcompiler "$(FOPENMP)" -std=c++11
diff --git a/make.inc-examples/make.inc.hip-gcc-openblas b/make.inc-examples/make.inc.hip-gcc-openblas
index 2c6be03ea..547169bb1 100644
--- a/make.inc-examples/make.inc.hip-gcc-openblas
+++ b/make.inc-examples/make.inc.hip-gcc-openblas
@@ -108,9 +108,6 @@ LDFLAGS     =     $(FPIC) $(FOPENMP)
 # add in the default for device compiling
 DEVCCFLAGS  = -O3 -DNDEBUG -DADD_
 
-# Add ROCM_VERSION for device compilation
-DEVCCFLAGS += -DROCM_VERSION=$(shell ./tools/get-rocm-version.sh)
-
 # add the flags in a backend-specific way
 ifeq ($(BACKEND),cuda)
     DEVCCFLAGS += -Xcompiler "$(FPIC)" -Xcompiler "$(FOPENMP)" -std=c++11
diff --git a/tools/get-rocm-version.sh b/tools/get-rocm-version.sh
deleted file mode 100755
index c28974a12..000000000
--- a/tools/get-rocm-version.sh
+++ /dev/null
@@ -1,68 +0,0 @@
-#!/bin/bash
-
-DEBUG=0
-[[ -d $1 ]] && ROCM_PATH=$1 || ROCM_PATH="${ROCM_PATH:-/opt/rocm}"
-HEADER_PATH_ONE=${ROCM_PATH}/include/rocm-core/rocm_version.h
-HEADER_PATH_TWO=${ROCM_PATH}/include/rocm_version.h
-PACKAGE_PATH_ONE=${ROCM_PATH}/.info/version-dev
-PACKAGE_PATH_TWO=${ROCM_PATH}/.info/version
-
-function parse_semver() {
-    local token="$1"
-    local major=0
-    local minor=0
-    local patch=0
-
-    if egrep '^[0-9]+\.[0-9]+\.[0-9]+' <<<"$token" >/dev/null 2>&1
-    then
-        # It has the correct syntax.
-        local n=${token//[!0-9]/ }
-        local a=(${n//\./ })
-        major=${a[0]}
-        minor=${a[1]}
-        patch=${a[2]}
-    fi
-
-    echo "$major $minor $patch"
-}
-
-if [[ -f ${HEADER_PATH_ONE} ]]
-then
-    ROCM_VERSION_DEV_RAW=$(grep ROCM_BUILD_INFO ${HEADER_PATH_ONE} | cut -d '"' -f 2)
-    if [[ "x$DEBUG" = "x1" ]]
-    then
-        echo "Found ${HEADER_PATH_ONE}, ROCM_VERSION_DEV_RAW=${ROCM_VERSION_DEV_RAW}"
-    fi
-elif [[ -f ${HEADER_PATH_TWO} ]]
-then
-    ROCM_VERSION_DEV_RAW=$(grep ROCM_BUILD_INFO ${HEADER_PATH_TWO} | cut -d '"' -f 2)
-    if [[ "x$DEBUG" = "x1" ]]
-    then
-        echo "Found ${HEADER_PATH_TWO}, ROCM_VERSION_DEV_RAW=${ROCM_VERSION_DEV_RAW}"
-    fi
-elif [[ -f ${PACKAGE_PATH_ONE} ]]
-then
-    ROCM_VERSION_DEV_RAW=$(cat ${PACKAGE_PATH_ONE})
-    if [[ "x$DEBUG" = "x1" ]]
-    then
-        echo "Found ${PACKAGE_PATH_ONE}, ROCM_VERSION_DEV_RAW=${ROCM_VERSION_DEV_RAW}"
-    fi
-elif [[ -f ${PACKAGE_PATH_TWO} ]]
-then
-    ROCM_VERSION_DEV_RAW=$(cat ${PACKAGE_PATH_TWO})
-    if [[ "x$DEBUG" = "x1" ]]
-    then
-        echo "Found ${PACKAGE_PATH_TWO}, ROCM_VERSION_DEV_RAW=${ROCM_VERSION_DEV_RAW}"
-    fi
-else
-    echo "ROCM_VERSION_NOT_FOUND"
-    exit 1
-fi
-
-a=($(parse_semver "${ROCM_VERSION_DEV_RAW}"))
-major=${a[0]}
-minor=${a[1]}
-patch=${a[2]}
-
-echo "$((major * 10000 + minor * 100 + patch))"
-
